({
	doInit : function(component, event, helper) {        
        component.set('v.columns', [
            {label: 'Contract Number', fieldName: 'ContractNumber', type: 'text'},
            {label: 'Account Name', fieldName: 'AccountName', type: 'text'},
        ]);        
        helper.getContract(component, helper);
    },

    onSave : function (component, event, helper) {
        helper.saveDataTable(component, event, helper);
    },
            
    /*openModel: function(component, event, helper) {
      console.log("openModel called");
      // for Display Model,set the "isOpen" attribute to "true"
      console.log(event.target.text);
      component.set("v.isOpen", true);
   },*/
 
   closeModel: function(component, event, helper) {
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
      console.log("closeModel called");
      component.set("v.isOpen", false);
   },
            
   /*closeModal:function(component,event,helper){    
        console.log("closeModal called");
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    },*/
            
    openmodal: function(component,event,helper) {
        console.log("openmodal called");
        console.log(event.target.text);
        component.set("v.contractNumber", event.target.text);
        component.set("v.isOpen", true);
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');   
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
    },
})