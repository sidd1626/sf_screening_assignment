({
    getContract : function(component, event, helper) {
        var action = component.get("c.getContract");
        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                var responseData = response.getReturnValue();
                var dataObject=[];
                for (let x=0; x < responseData.length; x++){
                    dataObject.push({ContractNumber: responseData[x].ContractNumber, AccountName: responseData[x].Account.Name});
                }
                component.set("v.data", dataObject);
            }
        });
        $A.enqueueAction(action);
    },
})