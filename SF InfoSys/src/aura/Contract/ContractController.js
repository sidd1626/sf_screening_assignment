({
    doInit : function(component, event, helper) {		                
        helper.getDataHelper(component, event);
    },
    delete : function(component, event, helper) {
    	var x = component.get('v.mydata');
    	for (let i=0; i<x.length; i++)
    		console.log(x[i]);
        component.set("v.isOpen", false);
	    if(confirm('Are you sure?')){
    		helper.deleteAssociation(component, event);
		}
    },
 
    addMore: function(component, event, helper){
   		console.log("addmore called");
        component.set("v.isOpen", true);
       // var cmpTarget = component.find('Modalbox');
        //var cmpBack = component.find('Modalbackdrop');   
        //$A.util.addClass(cmpTarget, 'slds-fade-in-open');
        //$A.util.addClass(cmpBack, 'slds-backdrop--open'); 	       
   	},
        
   closeModel: function(component, event, helper) {
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
      console.log("closeModel called");
      component.set("v.isOpen", false);
      var a = component.get('c.doInit');
      $A.enqueueAction(a);
   },

})