({
    getDataHelper : function(component, event) {
        var action = component.get("c.returnClauseWithContractNumber");
        action.setParams({contractNumber: component.get("v.contractNumber")});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                //component.set("v.mycolumns", [
                //    {label: 'Clause Name', fieldName: 'Name', type: 'text'},
            	//	{label: 'Type', fieldName: 'Type__c', type: 'text'},
                //    {type: 'action', typeAttributes: {rowAction: actions}}
                //]);
                console.log(response.getReturnValue());
                component.set("v.mydata", response.getReturnValue());

            }else if (state === 'ERROR'){
                var errors = response.getError();ss
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }else{
                console.log('Something went wrong, Please check with your admin');
            }
        });
        $A.enqueueAction(action);	
    },
    
    deleteAssociation : function(component, event, helper){
        console.log('Remove called');
        var action = component.get('c.deleteClauseAssignment');
        action.setParams({clauseID: event.target.id, contractNumber: component.get('v.contractNumber') });
        var currentData = component.get('v.mydata');
        for (let i=0; i<currentData.length; i++)
    		console.log(currentData[i]);
        for(let c=0; c< currentData.length; c++){
            if(currentData[c].id = event.target.id){
                console.log("Removing index: " + c);
                currentData.splice(c, 1);
                break;
            }
        }
        for (let i=0; i<currentData.length; i++)
    		console.log(currentData[i]);
        component.set('v.mydata', currentData);
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                if(response.getReturnValue() == false){
                    console.log('No clause assignment found with the given parameters. Failed!');
                }
                console.log('deleteAssociation ran successfully');
                //var a = component.get('c.getDataHelper');
       			//$A.enqueueAction(a);
            }else if (state === 'ERROR'){
                var errors = response.getError();ss
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }else{
                console.log('Something went wrong, Please check with your admin');
            }
            
        });
        $A.enqueueAction(action);
    }
})