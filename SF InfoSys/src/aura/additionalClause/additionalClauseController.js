({
	doInit : function(component, event, helper) {		                
        helper.getDataHelper(component, event);
    },
    
    checkboxSelect: function(component, event, helper){
        var selectedRec = event.getSource().get("v.value"); //checkboc boolean
        var getSelectedNumber = component.get("v.selectedCount");
        //let existingList = component.get("v.selectedClauseIDs");

        if(selectedRec){
            getSelectedNumber++;
        }else{
            getSelectedNumber--;
        }
        component.set("v.selectedCount", getSelectedNumber);
    },
    
    addSelected: function(component, event, helper) {
      // create var for store record id's for selected checkboxes  
      var addId = [];
      // get all checkboxes 
      var getAllId = component.find("boxPack");
      // If the local ID is unique[in single record case], find() returns the component. not array
         if(! Array.isArray(getAllId)){
             if (getAllId.get("v.value") == true) {
               addId.push(getAllId.get("v.text"));
             }
         }else{
         // play a for loop and check every checkbox values 
         // if value is checked(true) then add those Id (store in Text attribute on checkbox) in delId var.
         for (var i = 0; i < getAllId.length; i++) {
           if (getAllId[i].get("v.value") == true) {
             addId.push(getAllId[i].get("v.text"));
           }
          }
         } 
       
         // call the helper function and pass all selected record id's.    
         helper.addSelectedHelper(component, event, addId);
        console.log(addId);
        var parent = component.get("v.parentMethod");
        $A.enqueueAction(parent);
            
     },

     // For select all Checkboxes 
     selectAll: function(component, event, helper) {
      //get the header checkbox value  
      var selectedHeaderCheck = event.getSource().get("v.value");
      // get all checkbox on table with "boxPack" aura id (all iterate value have same Id)
      // return the List of all checkboxs element 
      var getAllId = component.find("boxPack");
      // If the local ID is unique[in single record case], find() returns the component. not array   
         if(! Array.isArray(getAllId)){
           if(selectedHeaderCheck == true){ 
              component.find("boxPack").set("v.value", true);
              component.set("v.selectedCount", 1);
           }else{
               component.find("boxPack").set("v.value", false);
               component.set("v.selectedCount", 0);
           }
         }else{
           // check if select all (header checkbox) is true then true all checkboxes on table in a for loop  
           // and set the all selected checkbox length in selectedCount attribute.
           // if value is false then make all checkboxes false in else part with play for loop 
           // and select count as 0 
            if (selectedHeaderCheck == true) {
            for (var i = 0; i < getAllId.length; i++) {
              component.find("boxPack")[i].set("v.value", true);
             component.set("v.selectedCount", getAllId.length);
            }
            } else {
              for (var i = 0; i < getAllId.length; i++) {
                component.find("boxPack")[i].set("v.value", false);
                 component.set("v.selectedCount", 0);
            }
           } 
         }  
     
     }
})