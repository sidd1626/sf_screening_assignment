({
	getDataHelper : function(component, event) {
        var action = component.get("c.returnAvailableClauses");
        action.setParams({contractNumber: component.get("v.contractNumber")});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                console.log(response.getReturnValue());
                component.set("v.mydata", response.getReturnValue());

            }else if (state === 'ERROR'){
                var errors = response.getError();ss
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }else{
                console.log('Something went wrong, Please check with your admin');
            }
        });
        $A.enqueueAction(action);	
    },
    
    
    addSelectedHelper: function(component, event, addId) {
          console.log("addSelectedHelper called");
          //call apex class method
          var action = component.get('c.createAssignment');
          // pass the all selected record's Id's to apex method 
          action.setParams({ addId: addId, contractNumber:  component.get("v.contractNumber")});
          action.setCallback(this, function(response) {
           //store state of response
           var state = response.getState();
           if (state === "SUCCESS") {
            console.log(state);
            console.log('check it--> delete successful');
            // call the onLoad function for refresh the List view    
            var a = component.get('c.doInit');
       		$A.enqueueAction(a);
            //component.set("v.mydata",
           }
          });
          $A.enqueueAction(action);
     },
 
})