trigger clauseAssignmentDuplicate on Clause_Assignment__c (before insert) {
    list<Clause_Assignment__c> existingAssignments = [select id, contract__c, contract_clause__c from Clause_Assignment__c Limit 50000];
    for(Clause_Assignment__c newAssignment: Trigger.NEW){
        for(Clause_Assignment__c existingAssignment: existingAssignments){
            if(newAssignment.Contract__c == existingAssignment.Contract__c &&
               newAssignment.Contract_Clause__c == existingAssignment.Contract_Clause__c){
                   newAssignment.addError('Clause Assignment already exist for the given contract and clause.');
               }
        }        
    }
}