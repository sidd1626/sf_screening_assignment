public class contractController {
	@AuraEnabled
    public static List<Contract> getContract(){
        return [Select contractNumber, Account.Name from Contract Limit 1000];
    }
    
    @AuraEnabled
    Public Static List<Contract_Clauses__c> returnClauseWithContractNumber(String contractNumber){
        List<clause_assignment__c> clauseAs = [select name, id, Contract_Clause__c from clause_assignment__c where contract__r.contractnumber = :contractNumber];
        list<ID> clauseAsID = new List<ID>();
        for (clause_assignment__c c: clauseAs){
            clauseAsID.add(c.Contract_Clause__c);
        }
        List<Contract_Clauses__c> clauses = [select id, name, description__c, type__c from Contract_clauses__c where id = :clauseAsID];
        system.debug(clauses);
        return clauses;
    }

    @AuraEnabled
    public static boolean deleteClauseAssignment(String clauseID, String contractNumber){
        List<Clause_Assignment__c> assignmentsForDelte = [select id, contract__c, contract_clause__c from Clause_Assignment__c where contract__r.contractNumber=: contractNumber and contract_clause__c = :clauseID ];
        system.debug('Clause Assignment for Delete: ' + assignmentsForDelte);
        delete assignmentsForDelte;
        return true;
    }
    
    
    @AuraEnabled
    public static list<contract_clauses__c> returnAvailableClauses(String contractNumber){
        List<clause_assignment__c> clauseAs = [select name, id, Contract_Clause__c from clause_assignment__c where contract__r.contractnumber = :contractNumber]; //return list of clause_assignmetn associated with a contract
        list<ID> clauseAsID = new List<ID>(); //List of ID's for Contract_Clause, that are already associated with the given contract. 
        for (clause_assignment__c c: clauseAs){
            clauseAsID.add(c.Contract_Clause__c);
        }
        List<Contract_Clauses__c> availableClauses = [select id, name, description__c, type__c from contract_clauses__c where id != :clauseAsID];
        return availableClauses;        
    }
    
    @AuraEnabled
    public static void createAssignment(List<String> addId, String contractNumber){
		ID contractId = [select id, contractNumber from Contract where contractNumber= :contractNumber limit 1].id;
        list<Clause_Assignment__c> newClauseAssignment= new list<Clause_Assignment__c>();
        for(String n: addId){
            newClauseAssignment.add(new Clause_assignment__c(contract__c = contractId , contract_clause__c = n));
        }
        system.debug(newClauseAssignment);
        insert newClauseAssignment;
    }
}