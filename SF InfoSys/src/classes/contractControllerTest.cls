@isTest
public class contractControllerTest {
	@isTest
    static void getContractTest(){
        Account acc = new Account(name='testing Account');
        insert acc;
        Contract contr = new Contract(accountId=acc.id, status='Draft', startDate=date.newInstance(2015, 12, 17), contractTerm=12);
        insert contr;
        List<Contract> lst = contractController.getContract();
        for(Contract c: lst){
            if(c.id == contr.Id){
                 system.assert(true);
            }
        }
    }
    
    @isTest
    Static void returnClauseWithContractNumberTest(){
        Account acc = new Account(name='testing Account');
        insert acc;
        Contract contr = new Contract(accountId=acc.id, status='Draft', startDate=date.newInstance(2015, 12, 17), contractTerm=12);
        insert contr;
        contr = [select id, contractNumber from Contract where id=:contr.Id][0];
        Contract_Clauses__c contrClause = new Contract_Clauses__c (name='testing clause', type__c='Financial', description__c = 'desc');
        insert contrClause;
        Clause_Assignment__c ClAs = new Clause_Assignment__c (contract__c = contr.Id, contract_Clause__c = contrClause.id);
        insert ClAs;
        List<Contract_Clauses__c> lst = contractController.returnClauseWithContractNumber(contr.ContractNumber);
        system.debug(lst[0].id + ' ' + contrClause.Id);
        System.assert(lst[0].id == contrClause.Id);
        system.assertNotEquals(null, lst);
    }
    
    @isTest
    Static void deleteClauseAssignmentTest(){
        Account acc = new Account(name='testing Account');
        insert acc;
        Contract contr = new Contract(accountId=acc.id, status='Draft', startDate=date.newInstance(2015, 12, 17), contractTerm=12);
        insert contr;
        contr = [select id, contractNumber from Contract where id=:contr.Id][0];
        Contract_Clauses__c contrClause = new Contract_Clauses__c (name='testing clause', type__c='Financial', description__c = 'desc');
        insert contrClause;
        Clause_Assignment__c ClAs = new Clause_Assignment__c (contract__c = contr.Id, contract_Clause__c = contrClause.id);
        insert ClAs;
        contractController.deleteClauseAssignment(contrClause.Id, contr.ContractNumber);  //the assingment should be deleted once the emthod is called.
        system.assert([select id, name from Clause_Assignment__c where id = :ClAs.Id].size() == 0); //after deleting the assignment the query should return null.
    }
    
    @isTest
    Static void returnAvailableClausesTest(){
        Account acc = new Account(name='testing Account');
        insert acc;
        Contract contr = new Contract(accountId=acc.id, status='Draft', startDate=date.newInstance(2015, 12, 17), contractTerm=12);
        insert contr;
        contr = [select id, contractNumber from Contract where id=:contr.Id][0];
        Contract_Clauses__c contrClause = new Contract_Clauses__c (name='testing clause', type__c='Financial', description__c = 'desc');
        insert contrClause;
        list<contract_clauses__c> lst = contractController.returnAvailableClauses(contr.ContractNumber);
        for (contract_clauses__c cc: lst){
            if(cc.id == contrClause.Id){
                system.assert(true);
            }
        }
        if (lst.size()<=0){
            system.assert(false);
        }
        Clause_Assignment__c ClAs = new Clause_Assignment__c (contract__c = contr.Id, contract_Clause__c = contrClause.id);
        insert ClAs;
        lst = contractController.returnAvailableClauses(contr.ContractNumber);
        system.assertNotEquals(0, lst.size());
    
    }
    
    @isTest
    static void createAssignmentTest(){
        Account acc = new Account(name='testing Account');
        insert acc;
        Contract contr = new Contract(accountId=acc.id, status='Draft', startDate=date.newInstance(2015, 12, 17), contractTerm=12);
        insert contr;
        contr = [select id, contractNumber from Contract where id=:contr.Id][0];
        List<Contract_Clauses__c> contrClause = new List<Contract_Clauses__c>();
        contrClause.add( new Contract_Clauses__c (name='testing clause', type__c='Financial', description__c = 'desc'));
        contrClause.add( new Contract_Clauses__c (name='testing clause', type__c='Financial', description__c = 'desc'));
        insert contrClause;
        List<String> clauseIDs = new List<String>();
        for (Contract_Clauses__c cc: contrClause){
            clauseIDs.add(cc.Id);
        }
        contractController.createAssignment(clauseIDs, contr.ContractNumber);
        list<Clause_assignment__c> lst = [select id, Contract_Clause__c, contract__c from Clause_assignment__c where contract__c = :contr.Id];
        system.assertEquals(2, lst.size());
        //system.assertNotEquals(null, lst.size());        
    }
    
    
}

/*

    @AuraEnabled
    public static void createAssignment(List<String> addId, String contractNumber){
		ID contractId = [select id, contractNumber from Contract where contractNumber= :contractNumber limit 1].id;
        list<Clause_Assignment__c> newClauseAssignment= new list<Clause_Assignment__c>();
        for(String n: addId){
            newClauseAssignment.add(new Clause_assignment__c(contract__c = contractId , contract_clause__c = n));
        }
        system.debug(newClauseAssignment);
        insert newClauseAssignment;
    }
} */