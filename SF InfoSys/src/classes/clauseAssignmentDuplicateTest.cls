@isTest
public class clauseAssignmentDuplicateTest {
    @isTest
    static void triggerTest(){
        Account acc = new Account(name='testing Account');
        insert acc;
        Contract contr = new Contract(accountId=acc.id, status='Draft', startDate=date.newInstance(2015, 12, 17), contractTerm=12);
        insert contr;
        contr = [select id, contractNumber from Contract where id=:contr.Id][0];
        Contract_Clauses__c contrClause = new Contract_Clauses__c (name='testing clause', type__c='Financial', description__c = 'desc');
        insert contrClause;
        Clause_Assignment__c ClAs = new Clause_Assignment__c (contract__c = contr.Id, contract_Clause__c = contrClause.id);
        insert ClAs;
        Clause_Assignment__c ClAsNew = new Clause_Assignment__c (contract__c = contr.Id, contract_Clause__c = contrClause.id);
        
        Test.startTest();
        	database.SaveResult sr = Database.insert(ClAsNew);
        Test.stopTest();
        System.assert(!sr.isSuccess());
        
    }
}